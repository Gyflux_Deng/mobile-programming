package com.sidm.mgp2018lab2;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.Touch;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;

public class GamePage extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //To request window feature
        requestWindowFeature(Window.FEATURE_NO_TITLE); //HIde title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(new GameView(this)); //surface view i gameview
    }

    @Override
    public  boolean onTouchEvent(MotionEvent event)
    {
        int posX = (int)event.getX();
        int posY = (int)event.getY();


        TouchManager.Instance.Update(posX, posY,event.getAction());
        return true;
    }
}
