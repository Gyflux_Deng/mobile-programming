package com.sidm.mgp2018lab2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceView;
import android.os.Build;

import java.util.Random;

public class SampleEntity implements EntityBase, Collidable
{
    //Init any variables here
    private boolean isDone = false;
    private boolean isInit = false;

    public static float xPlayer_Pos, yPlayer_Pos, lifeTime;
    //private Sprite spritesheet = null;
    private Sprite theSpritesheet[] = null;
    public static int coin ;
    public static boolean xtwo;
    public static  int countdown;
    public int currentMon;
    protected static final String TAG = null;

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone; // = done with whatever status or state you are now eg:On hit.
    }

    @Override
    public void Init(SurfaceView _view) {
        // Define anything you need to use here
        theSpritesheet = new Sprite[9];
        setSprite(_view);

        isInit = true;
        coin = 0;
        xtwo = false;
        countdown = 300;
        currentMon = MonsterPage.returnActiveMon();
    }
    public void setSprite(SurfaceView _view)
    {
        theSpritesheet[0] = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.monster1move), 1, 3, 16);
        theSpritesheet[1] = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.monster2move), 1, 4, 64);
        theSpritesheet[2] = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.monster3move), 1, 6, 48);
        theSpritesheet[3] = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.monster4move), 1, 6, 24);
        theSpritesheet[4] = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.monster5move), 1, 4, 48);
        theSpritesheet[5] = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.monster6move), 1, 8, 32);
        theSpritesheet[6] = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.monster7move), 1, 4, 20);
        theSpritesheet[7] = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.monster8move), 1, 8, 16);
        theSpritesheet[8] = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.monster9move), 1, 4, 12);
    }
    @Override
    public void Update(float _dt)
    {
        // Update based on dt

        if(xtwo == true)
        {
            countdown -= _dt;
        }
        if(countdown <= 0)
        {
            xtwo = false;
            countdown = 300;
        }

        if (TouchManager.Instance.IsDown())
        {
            //Check Collision here!!
          //  float imgRadius = spritesheet.GetHeight() * 0.5f;

                yPlayer_Pos -=10;
                theSpritesheet[MonsterPage.returnActiveMon()].Update(_dt);
                // Collided!
                //SetIsDone(true);
        }

        else if (TouchManager.Instance.HasRelease())
        {
            yPlayer_Pos +=10;
            theSpritesheet[currentMon].Update(_dt);
        }

    }

    @Override
    public void Render(Canvas _canvas)
    {
        // Render anything
        //spritesheet.Render(_canvas, (int) xPlayer_Pos, (int) yPlayer_Pos);
        theSpritesheet[MonsterPage.returnActiveMon()].Render(_canvas, (int) xPlayer_Pos, (int) yPlayer_Pos);
    }

    @Override
    public boolean IsInit() {
        return isInit;
    }

    public static SampleEntity Create()
    {
        SampleEntity result = new SampleEntity();
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    @Override
    public String GetType() {
        return "SampleEntity";
    }

    @Override
    public float GetPosX() {
        return xPlayer_Pos;
    }

    @Override
    public float GetPosY() {
        return yPlayer_Pos;
    }

    @Override
    public float GetRadius(){
        return theSpritesheet[currentMon].GetHeight() * 0.5f;
    }

    @Override
    public void OnHit(Collidable _other)
    {
        if(xtwo == false)
        {
            if (_other.GetType() == "BlockEntity")
            {
                coin +=1;
            }
        }
        else if (xtwo == true)
        {
            if (_other.GetType() == "BlockEntity")
            {
                coin +=2;
            }
        }


        if (_other.GetType() == "PowerupEntity")
        {
            xtwo = true;
        }
    }

}
