package com.sidm.mgp2018lab2;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.view.SurfaceView;

public class BlockEntity implements EntityBase, Collidable
{

    //Init any variables here

    protected static final String TAG = null;
    private Bitmap bmp = null;
    private Sprite spritesheet2 = null;
    private boolean isDone = false;
    private float xPos, yPos , lifeTime;;
    private int ScreenWidth, ScreenHeight;
    private boolean isInit = false;



    @Override
    public boolean IsDone()
    {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone)
    {
        isDone = _isDone;
    }

    @Override
    public void Init(SurfaceView _view) {

        // Define anything you need to use here

     //   lifeTime = 5.f;
        //public sprite animation(Bitmap _bmp, int col, int fps)
        spritesheet2  = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.coins), 1,5,20);
        //  bmp = BitmapFactory.decodeResource(_view.getResources(),R.drawable.ship2_1); //other images...

        DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
        ScreenWidth = metrics.widthPixels;
        ScreenHeight = metrics.heightPixels;

        isInit = true;

    }




    @Override
    public void Update(float _dt)
    {


//        lifeTime -= _dt;
//        if (lifeTime < 0.0f)
//            SetIsDone(true);
        spritesheet2.Update(_dt);

        xPos -=20;
//        // Update based on dt
//        if(TouchManager.Instance.IsDown())
//        {
//            //   float imgRadius = spritesheet.GetHeight() * 0.5f;
//            //   if(Collision.SphereToSphere(TouchManager.Instance.GetPosX(),TouchManager.Instance.GetPosY(),0.0f,xPos,yPos,imgRadius))
//            //  {
//            yPos -=30;
//            spritesheet.Update(_dt);
//            //   SetIsDone(true);
//            //   }
//
//        }
//        else if (TouchManager.Instance.HasRelease())
//        {
//            yPos +=10;
//            spritesheet.Update(_dt);
//        }
//



    }

    @Override
    public void Render(Canvas _canvas)
    {
        // Render anything
        spritesheet2.Render(_canvas,(int)xPos,(int)yPos);

    }

    @Override
    public boolean IsInit()
    {
        return isInit;
    }


    @Override
    public String GetType()

    {
        return "BlockEntity";
    }

    @Override
    public float GetPosX()

    {
        return xPos;
    }

    @Override
    public float GetPosY()

    {
        return yPos;
    }

    public static BlockEntity Create(float x_Pos, float y_Pos)
    {
        BlockEntity result = new BlockEntity();
        EntityManager.Instance.AddEntity(result);
        result.xPos = x_Pos;
        result.yPos = y_Pos;
        return result;
    }


    @Override
    public float GetRadius()
    {
        return spritesheet2.GetHeight() * 0.5f;
    }

    @Override
    public void OnHit(Collidable _other)
    {

        if (_other.GetType() == "SampleEntity")
        {
            SetIsDone(true);
         //   coin += 1;
        }

    }
}
