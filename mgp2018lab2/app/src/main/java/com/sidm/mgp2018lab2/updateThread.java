package com.sidm.mgp2018lab2;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.SurfaceHolder;


public class updateThread  extends Thread
{
    static final long targetFPS = 60;

    private GameView view = null;
    private SurfaceHolder holder = null;
    private boolean isRunning = false;

    public updateThread (GameView _view)
    {
        view = _view;
        holder = _view.getHolder();

        SampleGame.Instance.Init(view);
    }

    public boolean IsRunning()

    {
        return isRunning;
    }

    public void Initialize()

    {
        isRunning = true;
    }

    public void Terminate()

    {
        isRunning = false;
    }

    @Override
    public void run()
    {
        // Init variables here
        long framePerSecond = 1000 / targetFPS; // 1000 is milliseconds -> 1 second
        long startTime = 0;
        long prevTime = System.nanoTime();



        while(IsRunning())
        {
            // Update
            startTime = System.currentTimeMillis();

            // Get delta time
            long currTime = System.nanoTime();
            float deltaTime = (float)((currTime - prevTime) / 1000000000.0f);
            prevTime = currTime;
            // End delta time

            SampleGame.Instance.Update(deltaTime);



            // Render to happen, holder will lock the canvas
            // Canvas is a plain sheet of paper for u to draw or put images
            Canvas canvas = holder.lockCanvas();  // have to be lock before u can run or render things
            if (canvas != null) //  canvas cannot be empty
            {
                synchronized (holder) // Lock the door / ensure
                {
                    // Start to do render // BG color
                    canvas.drawColor(Color.BLACK);

                    // Game Scene
                    SampleGame.Instance.Render(canvas);

                }
                holder.unlockCanvasAndPost(canvas);
            }

            // Post Update
            try
            {
                long sleepTime = framePerSecond - (System.currentTimeMillis() - startTime);

                if (sleepTime > 0)
                    sleep(sleepTime);
            }
            catch(InterruptedException e)
            {
                Terminate();
            }
        }
    }
}