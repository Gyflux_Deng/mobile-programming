package com.sidm.mgp2018lab2;

public interface Collidable
{
        String GetType();

        float GetPosX();
        float GetPosY();

        float GetRadius();

        void OnHit(Collidable _other);
}
