package com.sidm.mgp2018lab2;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceView;

import java.util.LinkedList;

public class EntityManager
{
    public final static EntityManager Instance = new EntityManager();
    private LinkedList<EntityBase> entityList = new LinkedList<EntityBase>();
    private SurfaceView view = null;

    private EntityManager()
    {
    }

    public void Init(SurfaceView _view)
    {
        view = _view;
    }


    public void Update(float _dt)
    {

        LinkedList<EntityBase> removalList = new LinkedList<EntityBase>();

        // Update all
        for (EntityBase currEntity : entityList)
        {
            currEntity.Update(_dt);

            // Check if need to clean up
            if (currEntity.IsDone())
                removalList.add(currEntity);
        }

        // Remove all entities that are done
        for (EntityBase currEntity : removalList)
            entityList.remove(currEntity);

        removalList.clear(); // Clean up of removal list


        // Collision Check
        for (int i = 0; i < entityList.size(); ++i)
        {
            EntityBase currEntity = entityList.get(i);

            if (currEntity instanceof Collidable)
            {
                Collidable first = (Collidable) currEntity;

                for (int j = i+1; j < entityList.size(); ++j)
                {
                    EntityBase otherEntity = entityList.get(j);

                    if (otherEntity instanceof Collidable)
                    {
                        Collidable second = (Collidable) otherEntity;

                        if (Collision.SphereToSphere(first.GetPosX(), first.GetPosY(), first.GetRadius(), second.GetPosX(), second.GetPosY(), second.GetRadius()))
                        {
                           first.OnHit(second);
                           second.OnHit(first);
                        }
                    }
                }
            }

           //  Check if need to clean up
           if (currEntity.IsDone())
               removalList.add(currEntity);
        }

        // Remove all entities that are done
        for (EntityBase currEntity : removalList)
           entityList.remove(currEntity);
    }

    public void Render(Canvas _canvas)
    {
        for (EntityBase currEntity : entityList)
            currEntity.Render(_canvas);
    }

    public void AddEntity(EntityBase _newEntity)
    {
        _newEntity.Init(view);
        entityList.add(_newEntity);
    }
}

