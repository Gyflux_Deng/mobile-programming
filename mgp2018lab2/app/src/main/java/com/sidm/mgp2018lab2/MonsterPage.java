package com.sidm.mgp2018lab2;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;

public class MonsterPage extends Activity implements View.OnClickListener {

    private Button btn_back;

    public static int activeMon = 0;
    private Button mbtns[];


    public static int returnActiveMon () {
            return activeMon;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //if launching will create more than one
        //instance of this activity, bail out
        super.onCreate(savedInstanceState);
        //To request window feature
        requestWindowFeature(Window.FEATURE_NO_TITLE); //HIde title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.monsterpage);

        mbtns = new Button[9];

        // Set Listener to Buttons
        btn_back = (Button) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);

        mbtns[0] = (Button) findViewById(R.id.btn_M1);
        mbtns[0].setOnClickListener(this);
        mbtns[1] = (Button) findViewById(R.id.btn_M2);
        mbtns[1].setOnClickListener(this);
        mbtns[2] = (Button) findViewById(R.id.btn_M3);
        mbtns[2].setOnClickListener(this);
        mbtns[3] = (Button) findViewById(R.id.btn_M4);
        mbtns[3].setOnClickListener(this);
        mbtns[4] = (Button) findViewById(R.id.btn_M5);
        mbtns[4].setOnClickListener(this);
        mbtns[5] = (Button) findViewById(R.id.btn_M6);
        mbtns[5].setOnClickListener(this);
        mbtns[6] = (Button) findViewById(R.id.btn_M7);
        mbtns[6].setOnClickListener(this);
        mbtns[7] = (Button) findViewById(R.id.btn_M8);
        mbtns[7].setOnClickListener(this);
        mbtns[8] = (Button) findViewById(R.id.btn_M9);
        mbtns[8].setOnClickListener(this);

        mbtns[activeMon].setScaleX(1.3f);
        mbtns[activeMon].setScaleY(1.3f);

        setMon();
    }
    public void setMon()
    {
        if (GachaPage.returnMGet(2) == true)
        {
            //btn_m2.setBackgroundResource(R.drawable.monster2);
            mbtns[1].setBackgroundResource(R.drawable.monster2);
        }
        else
        {
            //btn_m2.setBackgroundResource(R.drawable.monster2nil);
            mbtns[1].setBackgroundResource(R.drawable.monster2nil);
        }
        if (GachaPage.returnMGet(3) == true)
        {
            //btn_m3.setBackgroundResource(R.drawable.monster3);
            mbtns[2].setBackgroundResource(R.drawable.monster3);
        }
        else
        {
            //btn_m3.setBackgroundResource(R.drawable.monster3nil);
            mbtns[2].setBackgroundResource(R.drawable.monster3nil);
        }
        if (GachaPage.returnMGet(4) == true)
        {
            //btn_m4.setBackgroundResource(R.drawable.monster4);
            mbtns[3].setBackgroundResource(R.drawable.monster4);
        }
        else
        {
            //btn_m4.setBackgroundResource(R.drawable.monster4nil);
            mbtns[3].setBackgroundResource(R.drawable.monster4nil);
        }
        if (GachaPage.returnMGet(5) == true)
        {
            //btn_m5.setBackgroundResource(R.drawable.monster5);
            mbtns[4].setBackgroundResource(R.drawable.monster5);
        }
        else
        {
            //btn_m5.setBackgroundResource(R.drawable.monster5nil);
            mbtns[4].setBackgroundResource(R.drawable.monster5nil);
        }
        if (GachaPage.returnMGet(6) == true)
        {
            //btn_m6.setBackgroundResource(R.drawable.monster6);
            mbtns[5].setBackgroundResource(R.drawable.monster6);
        }
        else
        {
            //btn_m6.setBackgroundResource(R.drawable.monster6nil);
            mbtns[5].setBackgroundResource(R.drawable.monster6nil);
        }
        if (GachaPage.returnMGet(7) == true)
        {
            // btn_m7.setBackgroundResource(R.drawable.monster7);
            mbtns[6].setBackgroundResource(R.drawable.monster7);
        }
        else
        {
            // btn_m7.setBackgroundResource(R.drawable.monster7nil);
            mbtns[6].setBackgroundResource(R.drawable.monster7nil);
        }
        if (GachaPage.returnMGet(8) == true)
        {
            // btn_m8.setBackgroundResource(R.drawable.monster8);
            mbtns[7].setBackgroundResource(R.drawable.monster8);
        }
        else
        {
            // btn_m8.setBackgroundResource(R.drawable.monster8nil);
            mbtns[7].setBackgroundResource(R.drawable.monster8nil);
        }
        if (GachaPage.returnMGet(9) == true)
        {
            //btn_m9.setBackgroundResource(R.drawable.monster9);
            mbtns[8].setBackgroundResource(R.drawable.monster9);
        }
        else
        {
            // btn_m9.setBackgroundResource(R.drawable.monster9nil);
            mbtns[8].setBackgroundResource(R.drawable.monster9nil);
        }
    }
    public void setActive(int MonID)
    {
        switch (MonID)
        {
            case 0:
            {
                if (activeMon != 0) {
                    mbtns[0].setScaleX(1.3f);
                    mbtns[0].setScaleY(1.3f);
                    mbtns[activeMon].setScaleX(1);
                    mbtns[activeMon].setScaleY(1);
                    activeMon = 0;
                }
                break;
            }
            case 1:
            {
                if (activeMon != 1) {
                    mbtns[1].setScaleX(1.3f);
                    mbtns[1].setScaleY(1.3f);
                    mbtns[activeMon].setScaleX(1);
                    mbtns[activeMon].setScaleY(1);
                    activeMon = 1;
                }
                break;
            }
            case 2:
            {
                if (activeMon != 2) {
                    mbtns[2].setScaleX(1.3f);
                    mbtns[2].setScaleY(1.3f);
                    mbtns[activeMon].setScaleX(1);
                    mbtns[activeMon].setScaleY(1);
                    activeMon = 2;
                }
                break;
            }
            case 3:
            {
                if (activeMon != 3) {
                    mbtns[3].setScaleX(1.3f);
                    mbtns[3].setScaleY(1.3f);
                    mbtns[activeMon].setScaleX(1);
                    mbtns[activeMon].setScaleY(1);
                    activeMon = 3;
                }
                break;
            }
            case 4:
            {
                if (activeMon != 4) {
                    mbtns[4].setScaleX(1.3f);
                    mbtns[4].setScaleY(1.3f);
                    mbtns[activeMon].setScaleX(1);
                    mbtns[activeMon].setScaleY(1);
                    activeMon = 4;
                }
                break;
            }
            case 5:
            {
                if (activeMon != 5) {
                    mbtns[5].setScaleX(1.3f);
                    mbtns[5].setScaleY(1.3f);
                    mbtns[activeMon].setScaleX(1);
                    mbtns[activeMon].setScaleY(1);
                    activeMon = 5;
                }
                break;
            }
            case 6:
            {
                if (activeMon != 6) {
                    mbtns[6].setScaleX(1.3f);
                    mbtns[6].setScaleY(1.3f);
                    mbtns[activeMon].setScaleX(1);
                    mbtns[activeMon].setScaleY(1);
                    activeMon = 6;
                }
                break;
            }
            case 7:
            {
                if (activeMon != 7) {
                    mbtns[7].setScaleX(1.3f);
                    mbtns[7].setScaleY(1.3f);
                    mbtns[activeMon].setScaleX(1);
                    mbtns[activeMon].setScaleY(1);
                    activeMon = 7;
                }
                break;
            }
            case 8:
            {
                if (activeMon != 8) {
                    mbtns[8].setScaleX(1.3f);
                    mbtns[8].setScaleY(1.3f);
                    mbtns[activeMon].setScaleX(1);
                    mbtns[activeMon].setScaleY(1);
                    activeMon = 8;
                }
                break;
            }
        }
    }
    @Override
    // A callback method
    public void onClick(View v) {
        //Intent = action to be performed
        //Intent is an object that you need to create a new instance to use it
        Intent intent = new Intent();

        if (v == btn_back) {
            intent.setClass(this, Mainmenu.class);
            finish();
        }
        else if (v == mbtns[0] ) {
            intent.setClass(this, MonsterPage.class);
            setActive(0);
        }
        else if (v == mbtns[1] ) {
            intent.setClass(this, MonsterPage.class);
            if (GachaPage.returnMGet(2) == true) {
                setActive(1);
            }
        }
        else if (v == mbtns[2] ) {
            intent.setClass(this, MonsterPage.class);
            if (GachaPage.returnMGet(3) == true) {
                setActive(2);
            }
        }
        else if (v == mbtns[3] ) {
            intent.setClass(this, MonsterPage.class);
            if (GachaPage.returnMGet(4) == true) {
                setActive(3);
            }
        }
        else if (v == mbtns[4] ) {
            intent.setClass(this, MonsterPage.class);
            if (GachaPage.returnMGet(5) == true) {
                setActive(4);
            }
        }
        else if (v == mbtns[5] ) {
            intent.setClass(this, MonsterPage.class);
            if (GachaPage.returnMGet(6) == true) {
                setActive(5);
            }
        }
        else if (v == mbtns[6] ) {
            intent.setClass(this, MonsterPage.class);
            if (GachaPage.returnMGet(7) == true) {
                setActive(6);
            }
        }
        else if (v == mbtns[7] ) {
            intent.setClass(this, MonsterPage.class);
            if (GachaPage.returnMGet(8) == true) {
                setActive(7);
            }
        }
        else if (v == mbtns[8] ) {
            intent.setClass(this, MonsterPage.class);
            if (GachaPage.returnMGet(9) == true) {
                setActive(8);
            }
        }
        startActivity(intent);
    }
}

