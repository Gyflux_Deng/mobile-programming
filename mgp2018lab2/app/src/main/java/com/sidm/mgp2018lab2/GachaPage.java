package com.sidm.mgp2018lab2;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Random;


public class GachaPage extends Activity implements View.OnClickListener  {

    //Define object button and pass it to another method to use

    private Button btn_back;
    private Button btn_gem;
    private Button btn_coin;
    private ImageView img;
    private TextView goldDisplay;
    //MediaPlayer bgm;


    private int premiumCost = 500;
    private int normalCost = 100;

    public static boolean M2Get = false;
    public static boolean M3Get = false;
    public static boolean M4Get = false;
    public static boolean M5Get = false;
    public static boolean M6Get = false;
    public static boolean M7Get = false;
    public static boolean M8Get = false;
    public static boolean M9Get = false;


    public static boolean returnMGet (int MID) {
        if (MID == 2) {
            return M2Get;
        }
        else if (MID == 3) {
            return M3Get;
        }
        else if (MID == 4) {
            return M4Get;
        }
        else if (MID == 5) {
            return M5Get;
        }
        else if (MID == 6) {
            return M6Get;
        }
        else if (MID == 7) {
            return M7Get;
        }
        else if (MID == 8) {
            return M8Get;
        }
        else if (MID == 9) {
            return M9Get;
        }
        else
        {
            return false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //if launching will create more than one
        //instance of this activity, bail out
        super.onCreate(savedInstanceState);
        //To request window feature
        requestWindowFeature(Window.FEATURE_NO_TITLE); //HIde title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.gachapage);
        // Set Listener to Buttons
        btn_back = (Button) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);
        btn_gem = (Button) findViewById(R.id.btn_gem);
        btn_gem.setOnClickListener(this);
        btn_coin = (Button) findViewById(R.id.btn_coin);
        btn_coin.setOnClickListener(this);
        img= (ImageView) findViewById(R.id.imageView);
        img.setImageResource(R.drawable.monster0);
        goldDisplay = (TextView)findViewById(R.id.goldView);
        goldDisplay.setText("Gold : " + Mainmenu.returnPlayerGold());

        //Uri.parse("android.resource:"+R.raw.bgm_music);
        //bgm.setDataSource(this,Uri);

    }
    int random;
    @Override
    // A callback method
    public void onClick(View v) {
        //Intent = action to be performed
        //Intent is an object that you need to create a new instance to use it
        Intent intent = new Intent();

        if (v == btn_back) {
            intent.setClass(this, Mainmenu.class);
            img= (ImageView) findViewById(R.id.imageView);
            img.setImageResource(R.drawable.monster0);
            Mainmenu.updateGold();
            finish();

        }
        else if (v == btn_gem ) {
            intent.setClass(this, GachaPage.class);
            if (Mainmenu.returnPlayerGold() >= premiumCost) {
                random = new Random().nextInt(99) + 1; // [0, 99] + 1 => [1, 100]
                img = (ImageView) findViewById(R.id.imageView);
                if (random < 25) {
                    img.setImageResource(R.drawable.monster6);
                    M6Get = true;
                } else if (random < 50) {
                    img.setImageResource(R.drawable.monster7);
                    M7Get = true;
                } else if (random < 80) {
                    img.setImageResource(R.drawable.monster8);
                    M8Get = true;
                } else {
                    img.setImageResource(R.drawable.monster9);
                    M9Get = true;
                }
                Mainmenu.setPlayerGold(Mainmenu.returnPlayerGold() - premiumCost);
            }
        }
        else if (v == btn_coin ) {
            intent.setClass(this, GachaPage.class);
            if (Mainmenu.returnPlayerGold() >= normalCost) {
                random = new Random().nextInt(99) + 1; // [0, 99] + 1 => [1, 100]
                img = (ImageView) findViewById(R.id.imageView);
                if (random < 25) {
                    img.setImageResource(R.drawable.monster2);
                    M2Get = true;
                } else if (random < 50) {
                    img.setImageResource(R.drawable.monster3);
                    M3Get = true;
                } else if (random < 75) {
                    img.setImageResource(R.drawable.monster4);
                    M4Get = true;
                } else {
                    img.setImageResource(R.drawable.monster5);
                    M5Get = true;
                }
                Mainmenu.setPlayerGold(Mainmenu.returnPlayerGold() - normalCost);
            }
            else
            {
                Mainmenu.setPlayerGold(1000);
            }
        }
        goldDisplay.setText("Gold : " + Mainmenu.returnPlayerGold());

        startActivity(intent);
    }
}
