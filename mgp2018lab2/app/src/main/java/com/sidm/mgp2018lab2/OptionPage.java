package com.sidm.mgp2018lab2;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;

public class OptionPage extends Activity implements View.OnClickListener {

    //Define object button and pass it to another method to use

    private Button btn_back;
    private Switch sw_sound;
    private SeekBar bar_mx;
    //MediaPlayer bgm;

    MediaPlayer mp ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //if launching will create more than one
        //instance of this activity, bail out
        super.onCreate(savedInstanceState);
        //To request window feature
        requestWindowFeature(Window.FEATURE_NO_TITLE); //HIde title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.optionpage);
        // Set Listener to Buttons
        btn_back = (Button) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);

        //Uri.parse("android.resource:"+R.raw.bgm_music);
        //bgm.setDataSource(this,Uri);


        // initiate a Switch
        sw_sound = (Switch)findViewById(R.id.sw_sound);
        if (sw_sound.isChecked())
        {
            sw_sound.setChecked(true);
        }
        else
        {
            sw_sound.setChecked(false);
        }
        sw_sound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (isChecked)
                {
                    play();
                    mp.setLooping(true);
                }
                else
                {
                    stopPlaying();
                }
            }
        });
        bar_mx = (SeekBar) findViewById(R.id.bar_music);
        bar_mx.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mp.setVolume(progress*0.1f,progress*0.1f);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        //play();
    }

    @Override
    // A callback method
    public void onClick(View v) {
        //Intent = action to be performed
        //Intent is an object that you need to create a new instance to use it
        Intent intent = new Intent();

        if (v == btn_back) {
            intent.setClass(this, Mainmenu.class);
        }
        startActivity(intent);
    }


    public void play() {
        if (mp == null) {
            mp = MediaPlayer.create(this, R.raw.bgm_music);
        }
        mp.start();
    }
    private void stopPlaying()
    {
        if (mp != null) {
            mp.stop();
           mp.release();
           mp = null;

        }
    }


    @Override
    protected void onDestroy() {
        if(mp != null)
        {
            mp.stop();
            mp.release();
            mp = null;
        }
       //remove this activity from the counter

        super.onDestroy();

    }
}

