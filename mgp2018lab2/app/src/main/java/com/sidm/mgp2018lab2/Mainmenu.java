package com.sidm.mgp2018lab2;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.telecom.Call;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import android.view.View;

import java.io.IOException;
import java.util.Arrays;



public class Mainmenu extends Activity implements OnClickListener {

    //Define object button and pass it to another method to use

    private Button btn_start;
    private Button btn_option;
    private Button btn_gatcha;
    private Button btn_farm;
    private Button btn_monster;

    private LoginButton login_Buttons;
    private TextView txt_status;
    private ShareDialog share_Dialog;
    CallbackManager callbackManager;
    private int PICK_IMAGE_REQUEST = 1;
    private static final String EMAIL = "email";
    // MediaPlayer mp;


    private static TextView goldDisplay;

    public static int thePlayerGold;

    public static void setPlayerGold (int gold)
    {
        thePlayerGold = gold;
    }


    public static int returnPlayerGold ()
    {
        return thePlayerGold;
    }
    public static void updateGold ()
    {
        goldDisplay.setText("Gold : " + thePlayerGold);
    }

   // MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //To request window feature
        requestWindowFeature(Window.FEATURE_NO_TITLE); //HIde title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        FacebookSdk.sdkInitialize(getApplicationContext());
        // AppEventsLogger.activateApp(this);
        setContentView(R.layout.mainmenu);

        // Set Listener to Buttons
        btn_start = (Button) findViewById(R.id.btn_start);
        btn_start.setOnClickListener(this);

        btn_option = (Button) findViewById(R.id.btn_option);
        btn_option.setOnClickListener(this);

        btn_gatcha = (Button) findViewById(R.id.btn_gatcha);
        btn_gatcha.setOnClickListener(this);

        btn_farm = (Button) findViewById(R.id.btn_farm);
        btn_farm.setOnClickListener(this);

        btn_monster = (Button) findViewById(R.id.btn_monster);
        btn_monster.setOnClickListener(this);

        login_Buttons = (LoginButton) findViewById(R.id.login_button);

        login_Buttons.setReadPermissions(Arrays.asList(EMAIL));
        // LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("public_profile"));
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));


        callbackManager = CallbackManager.Factory.create();
        txt_status = (TextView) findViewById(R.id.txt_status);
        callbackManager = CallbackManager.Factory.create();
        share_Dialog = new ShareDialog(this);

        goldDisplay  = (TextView) findViewById(R.id.goldView);
        goldDisplay.setText("Gold : " + thePlayerGold);

        login_Buttons.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                txt_status.setText("Login Boi");
                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                //   boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
                loginResult.getAccessToken().getUserId();

            }

            @Override
            public void onCancel() {

                txt_status.setText("Login Cancel");
            }

            @Override
            public void onError(FacebookException error) {
                txt_status.setText("Login Error");
            }


        });


    }

    public void sharePhotos(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_REQUEST && data != null && data.getData() != null) {

                Bitmap image = null;
                try {
                    image = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                SharePhoto photo = new SharePhoto.Builder()
                        .setBitmap(image)
                        .build();

                if (ShareDialog.canShow(SharePhotoContent.class))
                {
                    SharePhotoContent sharePhotoContent = new SharePhotoContent.Builder()
                            .addPhoto(photo)
                            .build();

                    share_Dialog.show(sharePhotoContent);
                }


                }
            }
        }




    @Override
    // A callback method
    public void onClick(View v){
        //Intent = action to be performed
        //Intent is an object that you need to create a new instance to use it

        Intent intent = new Intent();

        if(v == btn_start){
            intent.setClass(this,GamePage.class);

        }

        else if (v == btn_farm)
        {
            intent.setClass(this,FarmPage.class);

        }

        else if (v == btn_option)
        {

            intent.setClass(this,OptionPage.class);

        }
        else if (v == btn_gatcha)
        {
            intent.setClass(this,GachaPage.class);


        }

        else if (v == btn_monster)
        {
            intent.setClass(this,MonsterPage.class);
           // stopPlaying();
        }
        startActivity(intent);
    }



    public void play()
    {
       // if(mp == null)
      //  {
      //      mp = MediaPlayer.create(this, R.raw.bgm_music);
      //  }
       // mp.start();
    }




    @Override
    public void onDestroy()
    {

      //  if(mp != null)
       // {
      //      mp.stop();
       //     mp.release();
       //     mp = null;
       // }
        //remove this activity from the counter
        super.onDestroy();

    }

    private void stopPlaying()
    {
       /* if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }*/
    }





}
