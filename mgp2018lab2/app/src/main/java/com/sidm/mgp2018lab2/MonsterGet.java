package com.sidm.mgp2018lab2;

public interface MonsterGet {
    boolean mon1Get();
    boolean mon2Get();

    void SetMonGet(int monID, boolean isGet);

}
