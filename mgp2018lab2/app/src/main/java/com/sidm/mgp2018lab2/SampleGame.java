package com.sidm.mgp2018lab2;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.SurfaceView;

import java.util.LinkedList;
import java.util.Random;

//your stupid game
public class SampleGame
{
    //write a singleton
    public final static SampleGame Instance = new SampleGame();

    public DisplayMetrics metrics;

    //define object names
    private Bitmap bmp; // png = bitmap
    private Bitmap Scaledbmp;
    float offset = 0.0f;

    //week 6 screenwith and height
    int ScreenWidth, ScreenHeight,privateHeight;
    float Time,powerUpTime;
    float xPos = 0.0f, yPos = 0.0f; //variable used for position of image
    int xSpawnPos = 0, ySpawnPos = 0;
    int xPowerPos = 0, yPowerPos =0;


    private SampleGame()
    {

    }

    public void Init(SurfaceView _View)
    {
            bmp = BitmapFactory.decodeResource(_View.getResources(),R.drawable.gamemovement);
            metrics = _View.getResources().getDisplayMetrics();
            ScreenWidth = metrics.widthPixels;
            ScreenHeight = metrics.heightPixels;
            privateHeight = metrics.heightPixels;
            //scaled version of bitmap which is background of games scene;
            Scaledbmp = Bitmap.createScaledBitmap(bmp, 1920,1080,true);
        xSpawnPos = ScreenWidth;
        //new
        EntityManager.Instance.Init(_View);
        SampleEntity.Create();
        RenderTextEntity.Create();
        Time =5.f;
        powerUpTime = 5.f;
        SampleEntity.xPlayer_Pos = ScreenWidth/4.5f;


    }


    public void Update(float _deltaTime) {

        offset += _deltaTime;
        xPos -= _deltaTime * 500;
        Time -= _deltaTime;
        powerUpTime -= _deltaTime;
        if (xPos < -ScreenWidth) {
            xPos = 0;

        }
        EntityManager.Instance.Update(_deltaTime);

        if (Time <= 0) {
            //Random randY = new Random();
            int randy = (int) (Math.random() * (ScreenHeight));
            ySpawnPos = randy;
            BlockEntity.Create(xSpawnPos, ySpawnPos);

            Time = 1.f;
        }

        if (powerUpTime <= 0) {
            int powerRand = (int) (Math.random() * (ScreenHeight));
            yPowerPos = powerRand;
            PowerupEntity.Create(xSpawnPos, ySpawnPos);
            powerUpTime = 5.f;
        }


         if (SampleEntity.yPlayer_Pos > ScreenHeight)
        {
            SampleEntity.yPlayer_Pos = ScreenHeight;
        }
        if(SampleEntity.yPlayer_Pos < 100)
        {
            SampleEntity.yPlayer_Pos = 100;
        }





    }


    public void Render(Canvas _canvas)
    {
        int currOffset = (int) (offset * 100.f);

        //_canvas.drawBitmap(bmp,100,100,null);
      //  _canvas.drawBitmap(bmp,(10+ currOffset) %500,10,null);
        //left = x n top = y

        //week 6 draws something
        _canvas.drawBitmap(Scaledbmp,xPos,yPos,null);
        _canvas.drawBitmap(Scaledbmp,xPos+ScreenWidth,yPos,null);
        EntityManager.Instance.Render(_canvas);

        Paint paint = new Paint();
        paint.setARGB(225,0,0,0);
        paint.setStrokeWidth(200);
        paint.setTextSize(70);

        _canvas.drawText("Ypos: " +  ySpawnPos, ScreenWidth/2,80,paint);
        _canvas.drawText("Coin: " +  SampleEntity.coin, ScreenWidth/2,150,paint);
        _canvas.drawText("CountDown: " +  SampleEntity.countdown, ScreenWidth/2,220,paint);
        _canvas.drawText("PlayerPos: " +  SampleEntity.yPlayer_Pos, ScreenWidth/2,290,paint);

    }


}
