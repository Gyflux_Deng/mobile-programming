package com.sidm.mgp2018lab2;

import android.content.Context;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView {

    private SurfaceHolder holder = null;

    private  updateThread updateThread = new updateThread(this);

    public GameView(Context _context)
    {
        super(_context);
        holder = getHolder();

        if (holder != null)
        {
            holder.addCallback(new SurfaceHolder.Callback()
            {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {

                    if(!updateThread.IsRunning())
                        updateThread.Initialize();
                    if(!updateThread.isAlive())
                        updateThread.start();

                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

                    //n othing to type here cause it handle by the thread

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder)
                {
                    updateThread.Terminate();

                }
            });

        }


    }

}