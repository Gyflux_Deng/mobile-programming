package com.sidm.mgp2018lab2;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Random;

public class FarmPage extends Activity implements View.OnClickListener {

    private Button btn_back;

    private Button btn_Moles[];

    int ui = 100;

    int Row1 = 300;
    int Row2 = 500;
    int Row3 = 700;
    int Col1 = 500;
    int Col2 = 800;
    int Col3 = 1050;

    boolean slot[];

    int moleslot[];

    boolean gotNewPos = false;
    int moleBounty = 5;
    private static TextView gold;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //if launching will create more than one
        //instance of this activity, bail out
        super.onCreate(savedInstanceState);
        //To request window feature
        requestWindowFeature(Window.FEATURE_NO_TITLE); //HIde title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.farmpage);

        slot = new boolean[9];
        moleslot = new int[3];

        slot[0] = true;
        slot[1] = false;
        slot[2] = false;

        slot[3] = false;
        slot[4] = true;
        slot[5] = false;

        slot[6] = false;
        slot[7] = false;
        slot[8] = false;

        moleslot[0] = 0;
        moleslot[1] = 4;
        moleslot[2] = 8;

        btn_Moles = new Button[3];

        btn_back = (Button) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);

        btn_Moles[0] = (Button) findViewById(R.id.btn_mole0);
        btn_Moles[0].setOnClickListener(this);
        btn_Moles[1] = (Button) findViewById(R.id.btn_mole1);
        btn_Moles[1].setOnClickListener(this);
        btn_Moles[2] = (Button) findViewById(R.id.btn_mole2);
        btn_Moles[2].setOnClickListener(this);


        gold  = (TextView) findViewById(R.id.tv_gold);
        gold.setText("Gold : " + Mainmenu.returnPlayerGold());

    }

    void NewMoleLocation(int mole)
    {
        while (gotNewPos == false) {
            int random = new Random().nextInt(8) + 1; // [0, 8] + 1 => [1, 9]

            if (random == 1 && slot[0] == false) {
                btn_Moles[mole].setY(Row1);
                btn_Moles[mole].setX(Col1);
                slot[0] = true;
                slot[moleslot[mole]] = false;
                moleslot[mole] = random-1;
                gotNewPos = true;
            }
            else  if (random == 2 && slot[1] == false) {
                btn_Moles[mole].setY(Row1);
                btn_Moles[mole].setX(Col2);
                slot[1] = true;
                slot[moleslot[mole]] = false;
                moleslot[mole] = random-1;
                gotNewPos = true;
            }
            else  if (random == 3 && slot[2] == false) {
                btn_Moles[mole].setY(Row1);
                btn_Moles[mole].setX(Col3);
                slot[2] = true;
                slot[moleslot[mole]] = false;
                moleslot[mole] = random-1;
                gotNewPos = true;
            }
            else  if (random == 4 && slot[3] == false) {
                btn_Moles[mole].setY(Row2);
                btn_Moles[mole].setX(Col1);
                slot[3] = true;
                slot[moleslot[mole]] = false;
                moleslot[mole] = random-1;
                gotNewPos = true;
            }
            else  if (random == 5 && slot[4] == false) {
                btn_Moles[mole].setY(Row2);
                btn_Moles[mole].setX(Col2);
                slot[4] = true;
                slot[moleslot[mole]] = false;
                moleslot[mole] = random;
                gotNewPos = true;
            }
            else  if (random == 6 && slot[5] == false) {
                btn_Moles[mole].setY(Row2);
                btn_Moles[mole].setX(Col3);
                slot[5] = true;
                slot[moleslot[mole]] = false;
                moleslot[mole] = random-1;
                gotNewPos = true;
            }
            else  if (random == 7 && slot[6] == false) {
                btn_Moles[mole].setY(Row3);
                btn_Moles[mole].setX(Col1);
                slot[6] = true;
                slot[moleslot[mole]] = false;
                moleslot[mole] = random-1;
                gotNewPos = true;
            }
            else  if (random == 8 && slot[7] == false) {
                btn_Moles[mole].setY(Row3);
                btn_Moles[mole].setX(Col2);
                slot[7] = true;
                slot[moleslot[mole]] = false;
                moleslot[mole] = random-1;
                gotNewPos = true;
            }
            else  if (random == 9 && slot[8] == false) {
                btn_Moles[mole].setY(Row3);
                btn_Moles[mole].setX(Col3);
                slot[8] = true;
                slot[moleslot[mole]] = false;
                moleslot[mole] = random-1;
                gotNewPos = true;
            }
        }
    }
    @Override
    // A callback method
    public void onClick(View v) {
        //Intent = action to be performed
        //Intent is an object that you need to create a new instance to use it
        Intent intent = new Intent();

        if (v == btn_back) {
            intent.setClass(this, Mainmenu.class);
            Mainmenu.updateGold();
            finish();
        }
        else if (v == btn_Moles[0]) {
            NewMoleLocation(0);
            gotNewPos = false;
            Mainmenu.setPlayerGold(Mainmenu.returnPlayerGold() + moleBounty);
        }
        else if (v == btn_Moles[1]) {
            NewMoleLocation(1);
            gotNewPos = false;
            Mainmenu.setPlayerGold(Mainmenu.returnPlayerGold() + moleBounty);
        }
        else if (v == btn_Moles[2]) {
            NewMoleLocation(2);
            gotNewPos = false;
            Mainmenu.setPlayerGold(Mainmenu.returnPlayerGold() + moleBounty);
        }
        gold.setText("Gold : " + Mainmenu.returnPlayerGold());
        //gold.setText("End");

    }
}

